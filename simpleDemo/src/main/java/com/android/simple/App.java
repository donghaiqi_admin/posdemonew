package com.android.simple;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.android.simple.pay.BridgeModule;
import com.chinapnr.pos.device.PnrDevice;

import io.dcloud.application.DCloudApplication;
import io.dcloud.feature.uniapp.UniSDKEngine;
import io.dcloud.feature.uniapp.common.UniException;

public class App extends DCloudApplication {
    public static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        try {
            UniSDKEngine.registerModule("BridgeModule", BridgeModule.class);
        } catch (UniException e) {
            e.printStackTrace();
            Log.d("wlj", e.toString());
        }
        PnrDevice.getInstance(this).init();
    }
}
