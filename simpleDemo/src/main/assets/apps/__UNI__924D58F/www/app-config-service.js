
var isReady=false;var onReadyCallbacks=[];
var isServiceReady=false;var onServiceReadyCallbacks=[];
var __uniConfig = {"pages":["pages/index","pages/courseQueryConfirm","pages/paySelection","pages/createOrder","pages/orderPayment","pages/payResult","pages/orderDetail","pages/orderListByPhone"],"window":{"navigationBarTextStyle":"black","navigationBarTitleText":"示例工程","navigationBarBackgroundColor":"#F8F8F8","backgroundColor":"#F8F8F8"},"darkmode":false,"nvueCompiler":"uni-app","nvueStyleCompiler":"weex","renderer":"auto","splashscreen":{"alwaysShowBeforeRender":true,"autoclose":false},"appname":"POS","compilerVersion":"3.8.7","entryPagePath":"pages/index","networkTimeout":{"request":60000,"connectSocket":60000,"uploadFile":60000,"downloadFile":60000}};
var __uniRoutes = [{"path":"/pages/index","meta":{"isQuit":true},"window":{"navigationBarTitleText":"首页","navigationStyle":"custom","bounce":"none"}},{"path":"/pages/courseQueryConfirm","meta":{},"window":{"navigationBarTitleText":"课程查询确认","navigationStyle":"custom","bounce":"none"}},{"path":"/pages/paySelection","meta":{},"window":{"navigationBarTitleText":"支付选择","navigationStyle":"custom","bounce":"none"}},{"path":"/pages/createOrder","meta":{},"window":{"navigationBarTitleText":"填写订单","navigationStyle":"custom","bounce":"none"}},{"path":"/pages/orderPayment","meta":{},"window":{"navigationBarTitleText":"订单支付页面","navigationStyle":"custom","bounce":"none"}},{"path":"/pages/payResult","meta":{},"window":{"navigationBarTitleText":"支付结果","navigationStyle":"custom","bounce":"none"}},{"path":"/pages/orderDetail","meta":{},"window":{"navigationBarTitleText":"订单详情页面","navigationStyle":"custom","bounce":"none"}},{"path":"/pages/orderListByPhone","meta":{},"window":{"navigationBarTitleText":"用户的订单列表","navigationStyle":"custom","bounce":"none"}}];
__uniConfig.onReady=function(callback){if(__uniConfig.ready){callback()}else{onReadyCallbacks.push(callback)}};Object.defineProperty(__uniConfig,"ready",{get:function(){return isReady},set:function(val){isReady=val;if(!isReady){return}const callbacks=onReadyCallbacks.slice(0);onReadyCallbacks.length=0;callbacks.forEach(function(callback){callback()})}});
__uniConfig.onServiceReady=function(callback){if(__uniConfig.serviceReady){callback()}else{onServiceReadyCallbacks.push(callback)}};Object.defineProperty(__uniConfig,"serviceReady",{get:function(){return isServiceReady},set:function(val){isServiceReady=val;if(!isServiceReady){return}const callbacks=onServiceReadyCallbacks.slice(0);onServiceReadyCallbacks.length=0;callbacks.forEach(function(callback){callback()})}});
service.register("uni-app-config",{create(a,b,c){if(!__uniConfig.viewport){var d=b.weex.config.env.scale,e=b.weex.config.env.deviceWidth,f=Math.ceil(e/d);Object.assign(__uniConfig,{viewport:f,defaultFontSize:Math.round(f/20)})}return{instance:{__uniConfig:__uniConfig,__uniRoutes:__uniRoutes,global:void 0,window:void 0,document:void 0,frames:void 0,self:void 0,location:void 0,navigator:void 0,localStorage:void 0,history:void 0,Caches:void 0,screen:void 0,alert:void 0,confirm:void 0,prompt:void 0,fetch:void 0,XMLHttpRequest:void 0,WebSocket:void 0,webkit:void 0,print:void 0}}}});
