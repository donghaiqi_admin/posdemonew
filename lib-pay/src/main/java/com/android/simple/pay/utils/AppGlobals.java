package com.android.simple.pay.utils;

import android.app.Application;

import java.lang.reflect.Method;

public class AppGlobals {
    private static Application sApplication;

    public AppGlobals() {
    }

    public static Application getApplication() {
        if (sApplication == null) {
            try {
                Method currentApplication = Class.forName("android.app.ActivityThread").getDeclaredMethod("currentApplication");
                sApplication = (Application) currentApplication.invoke((Object) null);
            } catch (Exception var1) {
                var1.printStackTrace();
            }
        }

        return sApplication;
    }
}
