package com.android.simple.pay;

import android.util.Log;

import com.alibaba.fastjson.JSONObject;
import com.android.simple.pay.utils.AppGlobals;
import com.chinapnr.pos.config.InterfaceType;
import com.chinapnr.pos.config.PnrRequestKey;
import com.chinapnr.pos.config.PnrResponseKey;
import com.chinapnr.pos.trans.PnrService;
import com.chinapnr.pos.trans.PnrTransListener;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.sql.Time;
import java.util.Map;
import java.util.Timer;

import io.dcloud.feature.uniapp.annotation.UniJSMethod;
import io.dcloud.feature.uniapp.bridge.UniJSCallback;
import io.dcloud.feature.uniapp.common.UniModule;
import timber.log.Timber;

public class BridgeModule extends UniModule {
    String TAG = "BridgeModule";

    @UniJSMethod(uiThread = true)
    public void startScan() {
        Log.d(TAG, "调起原生方法: ");

        PnrService.getInstance(AppGlobals.getApplication()).doScan(false, new PnrTransListener() {
            @Override
            public void onResult(String response) {
                Timber.e(response);
                Map resultMap = new Gson().fromJson(response, Map.class);
                String responseCode = resultMap.get(PnrResponseKey.KEY_RESPONSECODE).toString();
                if ("00".equals(responseCode)) {
                    Timber.e("扫码成功:" + resultMap.get("code").toString());
                } else {
                    Timber.e("扫码失败:" + resultMap.get(PnrResponseKey.KEY_MESSAGE).toString());
                }
            }
        });
    }


    @UniJSMethod(uiThread = true)
    public void logD(JSONObject options) {
        Timber.d(options == null ? "null" : options.toJSONString());
    }

    @UniJSMethod(uiThread = true)
    public void doTrans(JSONObject options, UniJSCallback callback) {
        JsonObject jsonData = new JsonObject();
        jsonData.addProperty(PnrRequestKey.KEY_INTERFACE_TYPE, InterfaceType.Trans.IT_PAY);
        jsonData.addProperty("channelId", options.getString("channelId"));
        jsonData.addProperty("mobilePayType", options.getString("mobilePayType"));
        jsonData.addProperty("ordAmt", options.getString("ordAmt"));
        jsonData.addProperty("outOrdId", options.getString("outOrdId"));
        jsonData.addProperty("goodsDesc", options.getString("goodsDesc"));
        jsonData.addProperty("bgRetUrl", options.getString("bgRetUrl"));
        jsonData.addProperty("merPriv", options.getString("merPriv"));
        jsonData.addProperty("ordRemark", options.getString("ordRemark"));
        jsonData.addProperty("accSplitBunch", options.getString("accSplitBunch"));
        jsonData.addProperty("outPrintData", options.getString("outPrintData"));
        jsonData.addProperty("outPrintQrCode", options.getString("outPrintQrCode"));
        jsonData.addProperty("isDelayAcct", options.getString("isDelayAcct"));
        jsonData.addProperty("ordDesc", options.getString("ordDesc"));
        PnrService.getInstance(AppGlobals.getApplication())
                .doTrans(jsonData.toString(), new PnrTransListener() {
                    @Override
                    public void onResult(String response) {
                        Timber.d("交易结果：" + response);
                        Timber.d("callback = " + callback);
                        Map resultMap = new Gson().fromJson(response, Map.class);
                        String responseCode = resultMap.get(PnrResponseKey.KEY_RESPONSECODE).toString();
                        String merOrdId = resultMap.get(PnrResponseKey.KEY_MER_ORD_ID).toString();
                        Timber.d("responseCode =" + responseCode);

                        if (callback != null) {
                            JSONObject data = new JSONObject();
                            data.put("code", responseCode);
                            if ("00".equals(responseCode)) {
                                data.put("msg", "success");
                            } else {
                                data.put("msg", resultMap.get(PnrResponseKey.KEY_MESSAGE).toString());
                            }
                            data.put("merOrdId", merOrdId);
                            callback.invokeAndKeepAlive(data);
                            Timber.d("invoke success params=" + data.toJSONString());
                        }
                    }
                });

    }
}
